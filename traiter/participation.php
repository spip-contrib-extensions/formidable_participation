<?php

/**
 * Traitement participation à la saisie d'un formulaire
 *
 * @plugin     Formulaires de participation
 * @copyright  2014
 * @author     Anne-lise Martenot
 * @licence    GNU/GPL
 * @package    SPIP\Formidableparticipation\traiter\participation
 */


include_spip('inc/formidableparticipation');
include_spip('formidable_fonctions');
function traiter_participation_dist($args, $retours) {
	include_spip('base/abstract_sql');
	$statut_reponse = sql_getfetsel('statut', 'spip_formulaires_reponses', 'id_formulaires_reponse=' . $retours['id_formulaires_reponse']);

	// A partir de Formidable 7.0.0, la publication automatique d'une réponse passe par une action d'institution.
	// Donc on n'a pas à faire de traitement si jamais la réponse est publiée, mais uniquement si elle est moderée
	// En effet, si jamais la réponse est publiée, c'est formidableparticipation_post_edition qui se charge des choses
	// Seule exception : le cas où on modifie une réponse existante
	// @todo il faudrait simplifier tout cela, se faire une typologie des cas d'appel et voir si on ne peut pas ne traiter qu'à un seul endroit

	if (
		(
			$statut_reponse === 'prop'
			|| ($retours['modification_reponse'] ?? false)
		)
	) {
		$retours = traiter_participation_gerer($args, $retours);
	}

	// Dans le cas où l'on vient tout juste de créer la réponse qui est publié, il faut nettoyer les anciennes réponses
	if ($statut_reponse === 'publie' && !($retours['modification_reponse'] ?? false)) {
		formidable_participation_desinscription($retours['id_formulaires_reponse']);
	}

	// noter qu'on a deja fait le boulot, pour ne pas risquer double appel
	$retours['traitements']['participation'] = true;
	return $retours;
}

function traiter_participation_gerer($args, $retours) {
	$formulaire = $args['formulaire'];
	$options = $args['options'];
	$id_formulaire = $formulaire['id_formulaire'];
	$id_formulaires_reponse = $retours['id_formulaires_reponse'];
	if (isset($args['changement_statut'])) {
		$changement_statut = $args['changement_statut'];
	} else {
		$changement_statut = false;
	}


	// Initialisation des variable
	$choix_participation = '';
	$email_participation = '';
	$nom_participation = '';
	$prenom_participation = '';
	$organisme_participation = '';

	// saisies dans le formulaire
	if ($options['champ_choix_participation'] ?? '') {
		$champ = $options['champ_choix_participation'];
		$choix_participation = calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $champ, '', 'brut', '');
	}

	if ($options['champ_email_participation'] ?? '') {
		$champ = $options['champ_email_participation'];
		$email_participation = calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $champ, '', 'brut', '');
	}

	if (isset($options['champ_nom_participation']) ?? '') {
		$champ = $options['champ_nom_participation'];
		$nom_participation = calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $champ, '', 'brut', '');
	}

	if ($options['champ_prenom_participation'] ?? '') {
		$champ = $options['champ_prenom_participation'];
		$prenom_participation = calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $champ, '', 'brut', '');
	}


	$choix_participation = formidableparticipation_choix_participation($id_formulaire, $id_formulaires_reponse, $options, $changement_statut);

	// détermination de l'évènement où s'inscrire
	$id_evenement = formidableparticipation_id_evenement($id_formulaire, $id_formulaires_reponse, $options);

	// Nombre total d'inscription
	if ($options['plusieurs_fois'] ?? '') {
		$nb_inscriptions = 0;
		foreach ($options['champ_nb_inscriptions'] as $champ) {
			$nb_inscriptions = $nb_inscriptions + intval(calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $champ, '', 'brut', ''));
		}
	} else {
		$nb_inscriptions = 1;
	}
	foreach ($id_evenement as $evenement) {
		$options = [
			'id_evenement' => $evenement, //si oui, traitement avec agenda
			'choix_participation' => $choix_participation,
			'email' => $email_participation,
			'champ_evenement_participation' => $options['champ_evenement_participation'],
			'nom' => trim("{$prenom_participation} {$nom_participation}"),
			'nb_inscriptions' => $nb_inscriptions,
			'id_auteur' => (isset($GLOBALS['visiteur_session']['id_auteur']) ? $GLOBALS['visiteur_session']['id_auteur'] : 0),
			'id_formulaires_reponse' => $id_formulaires_reponse,
			'id_formulaire' => $args['id_formulaire'],
			'modification_reponse' => $retours['modification_reponse']
		];
		// Appel le pipeline traiter_formidableparticipation
		$pipeline = pipeline(
			'traiter_formidableparticipation',
			[
				'args' => array_merge(['args_traitement' => $args], $options),
				'data' => ''
			]
		);
	}


	return $retours;
}
