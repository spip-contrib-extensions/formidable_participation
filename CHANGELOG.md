# Changelog
## 6.1.1 - 2025-03-07

### Fixed

- Faire fonctionner le traitement dans le cas des préfixes autres que `_spip`

## 6.1.0 - 2025-03-01

### Added

- Pipeline `formidableparticipation_champs`

### Fixed

- Perf : ne pas appeler 6 fois le traitement pour rien
- Lorsque plusieurs personnes s'inscrivent avec le même email et qu'on dépublie une des réponses, ne pas modifier de la liste des participants une autre réponse que celle dont le statut est changée
- Permettre les mises à jour lorsqu'on réactive le plugin après une très long désactivation et qu'on utilise formidable v7
- Stocker le prénom en base de donnéee si jamais on a configuré pour

## 6.0.1 - 2025-01-24

### Fixed

- Refaire fonctionner le changement d'évènement à la modification d'une réponse existante

## 6.0.0 - 2025-01-22
### Changed

- Compatible FORMIDABLE 7.0.0 et > seulement

## 5.0.2 - 2024-10-21

### Changed

- Compatible SPIP 4.1 et supérieur seulement

### Fixed

- Ne pas desinscrire un email si la personnee reremplit le formulaire d'inscription à un evenement auquel elle était deja inscrite et que le formulaire est en modération a priori
## 5.0.1 - 2024-10-14
- Fatal à la mise à jour, lié à un bug de SPIP (spip/spip#5986)



### Remove

- Logos png


## 5.0.0 - 2024-08-25
### Remove

- Plusieurs paramètre supprimées lors de l'appel au pipeline `traiter_formidableparticipation`, voir le fichier `UPGRADE_5_0.md` pour les détails

### Fixed

- #25 Bugfix sur la maj vers la 4.0 dans certains cas
- #26 Lorsqu'on modifie une réponse, ne pas impacter les inscriptions pour les réponses aux même evenement avec le même email
- #27 Correction de libellés totalement incorrect

### Modified

- #24 Libellé et ergonomie du formulaire de configuration

## 4.1.1 - 2024-05-17

### Fixed

- Ne pas demander de configurer les champs de participation si la participation est automatique

## 4.1.0 - 2024-02-22

### Fixed

- #22 Se baser sur la cle `necessite` pour indiquer qu'il faut activer le traitement `enregistrer`
- Compatibilité formidable v6.0.0

## Removed

- Compatibilité formidable < v6.0.0
## 4.0.2 - 2023-12-01

### Fixed

- Ne pas provoquer d'erreur fatal lorsqu'on ne fournit pas d'évènement
## 4.0.1 - 2023-11-21

### Fixed

- Eviter page blanche/erreur de MAJ lorsqu'on met à jour via SVP

## 4.0.0 - 2023-10-27

### Added

- #17 Possibilité de déinscrire plusieurs places d'un coup (si même email).
### Changed

- #17 Par défaut un même email peut être inscrit plusieurs fois, plus besoin de cocher une case, si l'on veut n'utiliser qu'une seule fois un email à s'inscrire, option `unicite` du traitement `enregistrement` (livré avec Formidable). La migration est effectuée à la MAJ du plugin. Il n'y a donc plus d'option `autoriser_email_multiple`.
- #17 Pour désinscrire un email, on bascule le statut d'inscription de `oui` à `non`


## 3.1.1 - 2023-10-04

### Fixed

- #18 : logs erronnés
## 3.1.0 - 2023-09-22

### Added

- On peut utiliser un champ `hidden` pour le / les `id_evenement`

## 3.0.1 - 2023-06-15

### Fixed

- Les changements de statuts n'entrainaient plus d'inscription

## 3.0.0 - 2023-04-16

### Added

- Compatibilité SPIP 4

### Changed

- On ne peut plus configurer le traitement si le traitement "enregistrement" n'est pas activé

### Removed

- Compatibilité SPIP 3 à 3.2
## 2.1.0 - 2022-09-27

### Added

- #10 Possibilité d'avoir plusieurs champs contenant les évènements auxquels s'inscrire
- #10 Possibilité d'inscrire à plusieurs évènements fixes
- #11 Le pipeline `traiter_formidableparticipation` reçoit, lors de la modification d'une réponse, la liste des ancien·nes participant·es
- #11 Amélioration de la relation entre statut de la réponse et participation à l'évènement. Pour chaque statut on peut définir une règle:
    - `oui` : impose la participation
    - `non` : impose la non participation (par défaut pour les statuts `prop`, `poubelle`, `refuse`)
    - `auto` : dépend de la config du formulaire et du résultat de la réponse (par défaut pour statut `publie`)
    - `idem` : uniquement en cas de changement de statut : prendre l'ancienne valeur de participation.

    Le tout est personnalisable  par le pipeline `formidableparticipation_statuts_reponse_participation` si on ajoute des statuts / si on veut avoir des règles spéciales selon les formulaires / selon la réponse
- #11 L'email associé à la participation à l'évènement est trimé, pour éviter les espaces indésirables en BDD
### Changed

- #11 Désormais, que ce soit pour un changement de statut ou la modification d'un champ via les crayons, on passe par `traiter_participation_dist()` et par le pipeline afférant
- #6 Uniformisation du titre du traitement : on utilise un verbe, comme pour les autres traitements Formidable

### Fixed

- #11 Lorsque l'on appelle plusieurs fois le traitement dans un même hit et qu'il y a plusieurs réponses concernées, il faut supprimer les vieilles inscriptions pour toutes les réponses

## 2.0.4 - 2022-05-31

- Compatibilité Formidable 5.2.0

## 2.0.3 - 2022-04-01

### Added

- Compatibilité SPIP 4.1

### Bugfix

- Compatibilité avec les préfixes de table autres que `spip_`
