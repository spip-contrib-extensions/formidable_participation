<?php

/**
 * Utilisations de pipelines par Formulaires de participation
 *
 * @plugin     Formulaires de participation
 * @copyright  2014
 * @author     Anne-lise Martenot
 * @licence    GNU/GPL
 * @package    SPIP\Formidableparticipation\Pipelines
 */


if (!defined('_FORMIDABLE_PARTICIPATION_ACTUALISE_MAJ')) {
	define('_FORMIDABLE_PARTICIPATION_ACTUALISE_MAJ', false);
}
/**
 * Traiter les formulaires de participation
 * @param array $flux
 * @return array
 */
function formidableparticipation_traiter_formidableparticipation($flux) {
	//au moins une reponse et un email
	if ($flux['args']['choix_participation'] && $flux['args']['email']) {
		$id_evenement = intval($flux['args']['id_evenement'] ?? '');
		$reponse = $flux['args']['choix_participation'];
		$email = trim($flux['args']['email']);
		$id_auteur = $flux['args']['id_auteur'];
		$nom = $flux['args']['nom'];
		$nb_inscriptions = $flux['args']['nb_inscriptions'];
		$id_formulaires_reponse = $flux['args']['id_formulaires_reponse'];

		$champs = [
			'id_auteur' => $id_auteur,
			'nom' => $nom,
			'email' => $email,
			'reponse' => $reponse,
			'id_evenement' => $id_evenement,
			'date' => date('Y-m-d H:i:s'),
			'id_formulaires_reponse' => $id_formulaires_reponse
		];
		$champs = pipeline(
			'formidableparticipation_champs',
			[
				'args' => $flux['args'],
				'data' => $champs
			]
		);

		// Si c'est une mise à jour d'une réponse, on supprime TOUTES
		// les anciennes inscriptions avant de créer les nouvelles, mais on ne le fait qu'une fois
		static $suppression_faite = [];
		if (!in_array($id_formulaires_reponse, $suppression_faite)) {
			$flux['args']['evenements_participants_anciens'] = sql_allfetsel('*', 'spip_evenements_participants', "id_formulaires_reponse=$id_formulaires_reponse");// Passer aux suivant la liste des anciennes inscriptions avant leur suppression (pour éventuelle synchronisation).
			sql_delete('spip_evenements_participants', "id_formulaires_reponse=$id_formulaires_reponse");
			$suppression_faite[] = $id_formulaires_reponse;
		}
		formidable_participation_insertion($id_evenement, $nb_inscriptions, $champs);
	}

	return $flux;
}

/**
 * Lorsqu'une réponse est passée en refusée ou poubelle, changer le statut de l'inscription correspondant.
 * Réciproquement, lorsqu'une réponse est passée en validée, créer une inscription.
 * Lorsqu'un champ est modifié par crayons, modifier si besoin l'inscription.
 * @param array $flux
 * @return array $flux
 **/
function formidableparticipation_post_edition($flux) {

	$statut =  $flux['data']['statut'] ?? '';
	$statut_ancien = $flux['args']['statut_ancien'] ?? '';
	//Modification d'une réponse
	if (
		($flux['args']['table'] ?? '') === 'spip_formulaires_reponses'
		&& $flux['args']['action'] === 'instituer'
		&& $statut
		&& $statut_ancien
		&& $statut != $statut_ancien
	) {
		$id_formulaires_reponse = $flux['args']['id_objet'];
		$formulaire = sql_fetsel('F.*, R.id_formulaires_reponse', 'spip_formulaires AS F JOIN spip_formulaires_reponses AS R', "id_formulaires_reponse = $id_formulaires_reponse AND F.id_formulaire = R.id_formulaire");
	}
	// Modification d'un champ selon les crayons
	if (
		($flux['args']['type'] ?? '') === 'formulaires_reponses_champ'
		&& $flux['args']['action'] === 'modifier'
		&&  _request('action') === 'crayons_store' // Securité, pour ne pas appeler 36 fois si jamais un jour on passe aussi par objet_modifier dans traiter/enregistrement
	) {
		$id_formulaires_reponses_champ = $flux['args']['id_objet'];
		$formulaire = sql_fetsel(
			['f.*',  'r.id_formulaires_reponse'],
			['spip_formulaires AS f', 'spip_formulaires_reponses AS r', 'spip_formulaires_reponses_champs AS c'],
			["id_formulaires_reponses_champ = $id_formulaires_reponses_champ",  'r.id_formulaires_reponse = c.id_formulaires_reponse', 'f.id_formulaire = r.id_formulaire']
		);
	}
	// Si jamais on a bien trouvé un formulaire, c'est qu'on est bien sur une réponse, et donc on peut refaire tous le processus de participation
	if (isset($formulaire)) {
		$id_formulaires_reponse = $formulaire['id_formulaires_reponse'];
		unset($formulaire['id_formulaires_reponse']);

		include_spip('formidable_fonctions');
		$traitements = formidable_deserialize($formulaire['traitements']);

		// Si participation, on repasse au traitement
		if (isset($traitements['participation'])) {
			$options = $traitements['participation'];
			$args = [
				'formulaire'  => $formulaire,
				'id_formulaire' => $formulaire['id_formulaire'],
				'id_formulaires_reponse' => $id_formulaires_reponse,
				'options' => $traitements['participation'],
				'changement_statut' => $statut != $statut_ancien
			];
			$retours = [
				'traitements' => [],
				'id_formulaires_reponse' => $id_formulaires_reponse,
				'modification_reponse' => true
			];
			include_spip('traiter/participation');
			traiter_participation_gerer($args, $retours);
			spip_log("Crayonnage ou modification de statut sur $id_formulaires_reponse", 'formidable_participation');
		}

	}
	return $flux;
}

/**
 * - Sur ?exec=evenement, remplacer le lien vers ?exec=agenda_inscriptions vers la liste des réponses formidable, si les inscriptions se gèrent via formidable
 * - Sur ?exec=agenda_inscriptions renvoyer vers la liste des réponses formidables, si les inscriptions se gèrent via formidable
 * @param array $flux
 * @return array $flux
 **/
function formidableparticipation_affiche_milieu($flux) {
	$exec = $flux['args']['exec'];
	if ($exec === 'evenement' || $exec === 'agenda_inscriptions') {
		$data = &$flux['data'];
		if ($exec == 'evenement') {
			$regexp = '#<div><a href=".*\?exec=agenda_inscriptions.*<\/div>#U';#trouver le lien vers agenda_inscriptions
		} else {
			$regexp = '#<a name=\'pagination_inscrits\'.*#s';#trouver toutes les infos du tableau d'inscription
		}
		if (preg_match($regexp, $data, $match)) {
			if ($lien = formidableparticipation_liens_reponses_depuis_evenement($flux['args']['id_evenement'])) {
				if ($exec == 'agenda_inscriptions') {
					$lien = wrap(_T('agenda:liste_inscrits'), '<h2>') . $lien . '</div></div></div>';
				}
				$data = str_replace($match, $lien, $data);
			}
		}
	}
	return $flux;
}

/**
 * Sur ?exec=agenda_inscriptions, renvoyer vers les reéponses formulaires à gauche, si pertinent
 * @param array $flux;
 * @return $flux
 **/
function formidableparticipation_affiche_droite($flux) {
	if ($flux['args']['exec'] == 'agenda_inscriptions') {
		$data = &$flux['data'];
		if ($lien = formidableparticipation_liens_reponses_depuis_evenement($flux['args']['id_evenement'])) {
			$data = '';
		}
	}
	return $flux;
}
/**
 * Lien vers le formulaire lié à des inscriptions à un évènement, depuis un évènement
 * @param int|str $id_evenement
 * @return fond interprété, ou rien si jamais pas d'inscription par formidable
 **/
function formidableparticipation_liens_reponses_depuis_evenement($id_evenement) {
	$formulaires = sql_allfetsel(
		'f.id_formulaire',
		['spip_formulaires AS f', 'spip_formulaires_reponses AS r', 'spip_evenements_participants AS p'],
		['f.id_formulaire = r.id_formulaire', 'r.id_formulaires_reponse = p.id_formulaires_reponse', "p.id_evenement = $id_evenement"]
	);
	foreach ($formulaires as $cle => &$valeur) {
		$valeur = $valeur['id_formulaire'];
	}

	if (!$formulaires) {
		return '';
	}

	$lien = recuperer_fond('prive/squelettes/inclure/formidableparticipation_liens_reponses', ['formulaires' => $formulaires, 'id_evenement' => $id_evenement]);
	return $lien;
}
