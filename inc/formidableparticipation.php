<?php


include_spip('formidable_fonctions');
/**
 * Détermine au moment de le saisie du formulaire si la personne participe ou pas
 * @param int|val $id_formulaire
 * @param int|val $id_formulaires_reponse
 * @param array $options les options du traitement participation
 * @param bool $changement_statut true si on a affaire à un changement de statut de la réponse
 * @return str oui|non
 **/
function formidableparticipation_choix_participation($id_formulaire, $id_formulaires_reponse, $options, bool $changement_statut = false) {
	// Regarder s'il y a des statuts qui imposent un choix participation
	$statut_reponse = sql_getfetsel('statut', 'spip_formulaires_reponses', "id_formulaires_reponse=$id_formulaires_reponse");
	$statuts_reponse_participation = pipeline(
		'formidableparticipation_statuts_reponse_participation',
		[
			'args' => [
				'id_formulaire' => $id_formulaire,
				'id_formulaires_reponse' => $id_formulaires_reponse,
				'choix_participation' => $options['choix_participation_oui']
			],
			// 4 choix possibles
			// - oui
			// - non
			// - auto : selon la config du formulaire
			// - idem : prendre l'ancienne valeur de participation
			'data' => [
				'prop' => 'non',
				'poubelle' => 'non',
				'refuse' => 'non',
				'publie' => 'auto' // Si publie, on prend les règles de base
			]
		]
	);
	// Est-ce que c'est un statut qui impose quelque chose ?
	if (in_array($statuts_reponse_participation[$statut_reponse], ['oui', 'non'])) {
		return $statuts_reponse_participation[$statut_reponse];
	} elseif ($changement_statut === true && $statuts_reponse_participation[$statut_reponse] === 'idem') {//Dans le cas de changement de statut avec un statut qui dit de garder l'ancien choix
		return sql_getfetsel('reponse', 'spip_evenements_participants', "`id_formulaires_reponse`=$id_formulaires_reponse");
	}


	if ($options['participation_auto'] == 'auto') {
		$choix_participation = 'oui';
	} elseif ($options['champ_choix_participation'] ?? []) {
		$choix_participation = calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $options['champ_choix_participation'], '', 'brut', '');

		$participation_oui = $options['choix_participation_oui'];

		if ($choix_participation == $participation_oui) {
			$choix_participation = 'oui';
		} else {
			$choix_participation = 'non';
		}
	} else {
		$choix_participation = 'oui';
	}
	return $choix_participation;
}


/**
 * Détermine pour une réponse de formulaire les évènements associés
 * @param int|val $id_formulaire
 * @param int|val $id_formulaires_reponse
 * @param array $options les options du traitement participation
 * @return array tableau des $id_evenement
 **/
function formidableparticipation_id_evenement($id_formulaire, $id_formulaires_reponse, $options) {
	$id_evenement = [];
	if ($options['evenement_type'] === 'fixe') {
		$id_evenement = array_map('trim', explode(',', $options['id_evenement_participation']));
	} elseif ($options['evenement_type'] === 'variable' && isset($options['champ_evenement_participation'])) {
		$id_evenement = calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $options['champ_evenement_participation'], '', 'brut', '');
	} elseif ($options['evenement_type'] === 'multichamp' && isset($options['multichamp_evenement_participation'])) {
		$id_evenement = [];
		foreach ($options['multichamp_evenement_participation'] as $champ) {
			$valeur_champ = calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $champ, '', 'brut', '');
			if (!is_array($valeur_champ)) {
				$valeur_champ = [$valeur_champ];
			}
			$id_evenement = array_merge($id_evenement, $valeur_champ);
		}
	}
	if (!is_array($id_evenement)) {
		$id_evenement = [$id_evenement];
	}
	return $id_evenement;
}


/**
 * Inscrire une ou plusieurs information sur la participation.
 * Il peut s'agir de dire qu'on participe, ou au contraire qu'on ne participe pas, selon le contenu de `$champs['participation']`
 *
 * @param int $id_evenement
 * @param int $nb_inscriptions
 * @param array $champs champs decrivant la participation
**/
function formidable_participation_insertion(int $id_evenement, int $nb_inscriptions, array $champs): void {
	if (!$id_evenement) {
		return;
	}
	$id_auteur = $champs['id_auteur'];
	$id_formulaires_reponse = $champs['id_formulaires_reponse'];
	$reponse = $champs['reponse'];
	$email = $champs['email'];
	$i = 0;
	while ($i < $nb_inscriptions) {
		$i++;
		sql_insertq('spip_evenements_participants', $champs);
	}

	if (_FORMIDABLE_PARTICIPATION_ACTUALISE_MAJ) {
		sql_update('spip_evenements', ['maj' => 'NOW()'], "id_evenement=$id_evenement");
	}

	spip_log("pipeline evenement $id_evenement pour $email et id_auteur=$id_auteur et id_formulaires_reponse=$id_formulaires_reponse et reponse=$reponse ($nb_inscriptions fois)", 'formidable_participation');
}


/**
 * Cette fonction sert pour une fonctionnalité historique du plugin (mais encore officillement supportée et sans dépréciation prévue cf. pt 5 de la documentation).
 * Dans le cas où le formulaire propose le choix de la désinscription par re-soumission du formulaire.
 * Ex : une personne s'inscrit. La participation est à `oui`.
 * Elle re remplit le formulaire en demandant une désinscription : la participation devient `non`
 *
 * Techniquement il faut donc supprimer l'ancienne participation.
 * La présente fonction est donc appelée après soumission du formulaire sans modification de réponse.
 * Elle vérifie les inscriptions associées à la réponse. Si elles sont égale à `'non'`, alors elle cherche des inscriptions égales à `'oui'` associées au même email et les supprime.
 * Ceci permet de remplacer les anciennes inscriptions.
 *
 * Cette fonction est appelée dans `traiter_participation_dist()` après qu'une nouvelle réponse ait été enregistrée en base.
 **/
function formidable_participation_desinscription(int $id_formulaires_reponse) {
	include_spip('base/abstract_sql');
	// Retrouvons ce qu'on vient juste de mettre
	$inscriptions_non = sql_select('email,id_evenement,id_evenement_participant', 'spip_evenements_participants', ["id_formulaires_reponse = $id_formulaires_reponse", 'reponse = \'non\'']);
	$nb_inscriptions_non = sql_count($inscriptions_non);

	if (!$nb_inscriptions_non) {
		return;
	}


	while ($row = sql_fetch($inscriptions_non)) {
		$email = $row['email'];
		$id_evenement_participant = $row['id_evenement_participant'];
		if (!$email) {
			break;
		}
		$id_evenement_participant_to_be_deleted = sql_getfetsel(
			'id_evenement_participant',
			'spip_evenements_participants',
			[
				'id_evenement = ' . $row['id_evenement'],
				'email = ' . sql_quote($row['email']),
				'reponse = \'oui\''
			],
			'',
			'',
			'0,1'
		);
		sql_delete('spip_evenements_participants', "id_evenement_participant = {$id_evenement_participant_to_be_deleted}");
		spip_log("Sur réponse {$id_formulaires_reponse},
			suppression de la participation {$id_evenement_participant_to_be_deleted}
			pour cause de désinscription,
			nouvelle participation {$id_evenement_participant}",
			'formidable_participation'
		);
	}
}

