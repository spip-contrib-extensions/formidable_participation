<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-formidableparticipation?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// F
	'formidableparticipation_description' => 'Strumento che ti consente di generare una pipeline di partecipazione quando inserisci una form del plug-in Formidable',
	'formidableparticipation_nom' => 'Modulo di partecipazione Formidable',
	'formidableparticipation_slogan' => 'Estensione del plugin Formidable',
];
