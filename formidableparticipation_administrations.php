<?php

/**
 * Fichier gérant l'installation et désinstallation du plugin Formulaires de participation
 *
 * @plugin     Formulaires d'participation
 * @licence    GNU/GPL
 * @package    SPIP\Formidableparticipation\Installation
 */



include_spip('inc/cextras');
include_spip('base/formidableparticipation');
/**
 * Fonction d'installation et de mise à jour du plugin Formulaires de participation.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 **/
function formidableparticipation_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];
	$creation_champ = ['sql_alter', 'TABLE spip_evenements_participants ADD column id_formulaires_reponse BIGINT(21)'];
	$maj['install'] = [$creation_champ];
	$maj['1.1.0'] = [
		['formidableparticipation_upgrade_1_1_0']
	];
	$maj['1.4.0'] = [$creation_champ];

	$maj['1.5.2'] = [
		['formidableparticipation_upgrade_1_5_2']
	];
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Met à jour vers le schema 1.1.0 des traitements participations.
 * A savoir, prend les traitements de participation existants et ajoute automatiquement
 * participation_auto = 'variable'
 * evenement_type = 'fixe'
 * Ceci permet de ne pas casser les évènements
 **/
function formidableparticipation_upgrade_1_1_0() {
	include_spip('inc/sql');
	include_spip('inc/formidable');
	$res = sql_select('id_formulaire,traitements', 'spip_formulaires');
	while ($row = sql_fetch($res)) {
		$traitements = formidable_participation_deserialize($row['traitements']);
		if (isset($traitements['participation'])) {
			$participation = &$traitements['participation'];
			if (!isset($participation['participation_auto'])) {
				$participation['participation_auto'] = 'variable';
			}
			if (!isset($participation['evenement_type'])) {
				$participation['evenement_type'] = 'fixe';
			}
			$traitements = serialize($traitements);
			sql_updateq('spip_formulaires', ['traitements' => $traitements], 'id_formulaire=' . $row['id_formulaire']);
		}
	}
}

/**
 * Met à jour le schéma vers 1.5.2
 * La case "Permettre à une même adresse email de s'inscrire plusieurs fois",
 * si elle n'est pas cochée, alors on prend le champ d'adresse email, et on dit que c'est un champ qui doit être unique (traiter/enregistrement/unicite)
**/
function formidableparticipation_upgrade_1_5_2(): void {
	include_spip('inc/sql');
	include_spip('inc/formidable');
	include_spip('formidable_fonctions');

	$res = sql_select('id_formulaire,traitements', 'spip_formulaires');
	while ($row = sql_fetch($res)) {
		$traitements = formidable_participation_deserialize($row['traitements']);
		$participation = $traitements['participation'] ?? '';
		if ($participation) {
			$autoriser_email_multiple = $participation['autoriser_email_multiple'] ?? '';
			if (!$autoriser_email_multiple) {
				$traitements['enregistrement']['unicite'] = $traitements['participation']['champ_email_participation'];
			}
			unset($participation['autoriser_email_multiple']);

			$traitements['participation'] = $participation;
			$traitements = formidable_participation_serialize($traitements);

			sql_updateq('spip_formulaires', ['traitements' => $traitements], 'id_formulaire=' . $row['id_formulaire']);

		}
	}
}

/**
 * Fonction de désinstallation du plugin Formulaires de participation.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 **/
function formidableparticipation_vider_tables($nom_meta_base_version) {
	sql_alter('TABLE spip_evenements_participants DROP column id_formulaires_reponse');
	effacer_meta($nom_meta_base_version);
}

// Contournement de bug de SVP. Cf https://git.spip.net/spip-contrib-extensions/formidable_participation/-/issues/25
function formidable_participation_serialize(array $tab): string {
	return json_encode($tab);
}

function formidable_participation_deserialize($texte) {
	if (is_null($texte)) {
		$texte = '';
	}
	// Cas 1. Deja tableau
	if (is_array($texte)) {
		return $texte;
	}
	// Cas 2. Tableau serializé en json
	$tmp = json_decode($texte, true);
	if (is_array($tmp)) {
		return $tmp;
	}
	// Cas 3. Tableau serializé en PHP, si jamais ca echout on renvoie le texte
	$tmp = @unserialize($texte);
	if (is_array($tmp)) {
		return $tmp;
	}
	return $texte;
}
