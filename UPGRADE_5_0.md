# Mise à jour vers la version 5.0 depuis une version 4.x

La version 5.0 nettoie un certain nombre de code mort et inutilisé. Ce sont essentiellement des paramètres supplémentaires passées aux fonctions d'enregistrement, mais jamais utilisée en pratique.

Les personnes qui les utilisaient dans leurs propres pipelines/fonctions trouveront ci-dessous comment faire.

## Organisme

L'option `Champ de l'organisme` qui n'était pas utilisé en interne est suppriméee.

Les personnes peuvent utiliser le plugin [Formidable participation : champ « organisme »](https://contrib.spip.net/5585)

## `tracking_id`

Le paramètre `tracking_id` n'est plus passé au pipeline `traiter_formidableparticipation`. La valeur se trouve à l'identique dans `id_formulaires_reponse`.

## `parrain`

Le paramètre `parrain` n'est plus passé au pipeline `traiter_formidableparticipation`. On peut le reconstruire à partir du paramètre `id_formulaire`
